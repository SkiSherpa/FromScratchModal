import React from 'react';
import ReactDOM from 'react-dom';
import './modal2.css';

// Use the "title", so the title is dynamic,
// but is determined by the parent component

// create portal, 1st need to add
// <div id="portal"></div>
// to the index.html file after root
// createPortal takes 2 argus.
// 1st the jsx to be returned
// 2nd the div with the id="portal"

// ! createPortal is working, but can still tab out of the modal
// maybe use "useRef" to fix?
export default function Modal2 ({isOpen, onClose, title, body}) {
    return ReactDOM.createPortal(
        isOpen && (
                <div className="overlay">
                    <div className="modal">
                        <h2 className="modal-title">{title}</h2>
                        <p className="modal-body">{body}</p>
                        <button className="modal-close-btn" onClick={onClose}>
                            CLOSE MODAL
                        </button>
                    </div>
                </div>
            )
            ,
            document.getElementById("portal")
    )
}
