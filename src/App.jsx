import { useState } from 'react';
import './App.css';
import Modal2 from './modal2/modal2.jsx';

function App() {

  const [ isOpen, setIsOpen ] = useState(false);
  const [ isOpen2, setIsOpen2 ] = useState(false);

  const bodyText = "This is the test body, I don't think that useState is needed as we don't update this text for this excersice. Adding more for commit"

  function toggleModal() {
    setIsOpen(!isOpen);
  }

  function toggleModal2() {
    setIsOpen2(!isOpen2);
  }

  return (
    <>
      <h1>Modal Practice</h1>
      <button className="btn-modal" onClick={() => toggleModal()}>Open Modal</button>
      <Modal2 title="Modal Componet 2, using notes" body={bodyText} isOpen={isOpen} onClose={toggleModal}/>
      {/* Can pass in the title or any content as props */}

      <button className="btn-modal2" onClick={() => toggleModal2()}>Open Modal Test 2</button>
      <Modal2 title="DIFFERENT TITLE 2" body="2 different body text?" isOpen={isOpen2} onClose={toggleModal2}/>
    </>
  );
}

export default App;

// Things to learn:
// import {createPortal} from 'react-dom';
// ^^ to control z index of modal, always bring to front
// resusability
// ^^ like passing the title and body as props, we should be able to make a second modal using modal2
